Source: vitetris
Section: games
Priority: optional
Maintainer: Baptiste Beauplat <lyknode@debian.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.7.0
Homepage: https://www.victornils.net/tetris/
Vcs-Browser: https://salsa.debian.org/debian/vitetris
Vcs-Git: https://salsa.debian.org/debian/vitetris.git
Rules-Requires-Root: no

Package: vitetris
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Virtual terminal *tris clone
 Vitetris is a terminal-based Tetris game. It can be played by one or two
 players, over the network or on the same keyboard.
 .
 Vitetris comes with more features and options than might be expected from
 a simple text mode game. Full input control, customizable appearance,
 netplay where both players can choose difficulty (level and height) --
 unless you must have sound (or just don't like Tetris), you won't be
 disappointed.
 .
 Rotation, scoring, levels and speed should resemble the early Tetris games
 by Nintendo, with the addition of a short lock delay which makes it
 possible to play at higher levels. (It does not make it possible to
 prevent the piece from ever locking by abusing lock delay resets.)
